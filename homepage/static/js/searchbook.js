// Kolaborator: Leonardo(1806191023). Terima kasih atas pelajaran AJAX nya

function search(keyword){
    $("#result")[0].innerHTML = "<p class='m-auto mt-3 mb-3'>Tunggu sebentar...<p>"
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + keyword,
        success: function(result){
            $("#result")[0].innerHTML = ""
            for (i = 0; i < result.items.length; i++){
                $("#result")[0].innerHTML += books(result.items[i]);
            }
        }
    })
}

function books(book){
    return (
       "<a href=" + book.volumeInfo.infoLink + " target='blank' class='m-auto'><div class='item m-4'>" +
        "<img src=" + book.volumeInfo.imageLinks.thumbnail + " class='mt-3 mr-3 ml-3'/>" +
        "<div class='book-title aligncustom'><p class='p-1 m-0'>" + book.volumeInfo.title + 
        ", Penulis: " + book.volumeInfo.authors +"</p></div></div></a>"
    )
}

$('#search').keypress(function(event){
    search(event.target.value);
});